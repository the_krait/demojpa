CREATE TABLE STUDENT (ID INT PRIMARY KEY, NAME VARCHAR(20), ADDR VARCHAR(20), TECH VARCHAR(20));
insert into student values (1,'ram','pune','java');
insert into student values (2,'sham','mumbai','sql');
insert into student values (3,'aman','chennai','c++');
insert into student values (4,'ganesh','hyderabad','golang');
insert into student values (5,'neeraj','mumbai','android');
insert into student values (6,'navin','kolkata','java');