package com.example.demo.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizeExceptionHandler extends ResponseEntityExceptionHandler {

	ExceptionResponse response = new ExceptionResponse();
	@ExceptionHandler(IdNotFoundException.class)
	public ResponseEntity<Object> handleExceptions( IdNotFoundException exception, WebRequest webRequest) {
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Id Not Found");
        ResponseEntity<Object> entity = new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        return entity;
    }
	
	@ExceptionHandler(NameNotFoundException.class)
	public ResponseEntity<Object> handleExceptions( NameNotFoundException exception, WebRequest webRequest) {
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Name Not Found");
        ResponseEntity<Object> entity = new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        return entity;
    }
	
	@ExceptionHandler(TechNotFoundException.class)
	public ResponseEntity<Object> handleExceptions( TechNotFoundException exception, WebRequest webRequest) {
        response.setDateTime(LocalDateTime.now());
        response.setMessage("Tech Not Found");
        ResponseEntity<Object> entity = new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        return entity;
    }
	
	
}
