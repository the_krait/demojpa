package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.IdNotFoundException;
import com.example.demo.exception.NameNotFoundException;
import com.example.demo.exception.TechNotFoundException;
import com.example.demo.model.Student;
import com.example.demo.service.StudentService;

@RestController
public class StudentController {

	@Autowired
	StudentService studentService;  //service calls

	@RequestMapping("student")
	public String addStudent(@RequestBody Student student) {
		studentService.save(student);
		return "home.jsp";
	}

	@RequestMapping("/studentbyid/{id}")
	public Student findById(@PathVariable(required=true) int id) {
		Student student = studentService.findById(id);
		if(student==null) {
			throw new IdNotFoundException();
		}
		return student;
	}
	
	@GetMapping("/studentbyname/{name}")
	public Student findByName(@PathVariable String name) {
		Student student = studentService.findByName(name);
		if(student==null) {
			throw new NameNotFoundException();
		}
		return student;
	}
	
	@GetMapping("/studentbytech/{tech}")
	public Student findByTech(@PathVariable String tech) {
		Student student = studentService.findByTech(tech);
		if(student==null) {
			throw new TechNotFoundException();
		}
		return student;
	}
	
	@GetMapping("/allstudents")
	public List<Student> findAllStudents() {
		return studentService.findAll();
	}

	@GetMapping("/studentorderbyasc/{name}") 
	public List<Student> findByIdOrderByIdAsc(@PathVariable String name){ 
		return studentService.findByNameOrderByNameAsc(name);
	}
	 
	@GetMapping("/studentidortech/{id}/{tech}")
	public Student findByIdOrTech(@PathVariable int id, String tech) {
		return studentService.findByIdOrTech(id, tech);
	}

	@GetMapping("/studentidandname/{id}/{name}")
	public Student findByIdAndName(@PathVariable int id, String name) {
		return studentService.findByIdAndName(id, name);
	}
	
	@GetMapping("/studentnamelike/{name}")
	public Student findByNameLike(@PathVariable String name) {

		String likepattern = "'%'" + name + "'%'";
		return studentService.findByNameLike(likepattern);
	}
	
	@GetMapping("/studentcount")
	public long countStudents() {
		return studentService.count();
	}
}
