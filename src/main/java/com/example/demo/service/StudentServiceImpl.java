package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;

@Component
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepository studentRepository;
	
	@Override
	public Student save(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Student findById(int id) {
		return studentRepository.findById(id).orElse(new Student());
	}

	@Override
	public Student findByName(String name) {
		return studentRepository.findByName(name);
	}

	@Override
	public Student findByTech(String tech) {
		return studentRepository.findByTech(tech);
	}

	@Override
	public List<Student> findAll() {
		return studentRepository.findAll();
	}

	@Override
	public List<Student> findByNameOrderByNameAsc(String name) {
		//List<Student> arrayList = new ArrayList<Student>();
		//arrayList.addAll(studentRepository.findByNameOrderByNameAsc(name));
		//return arrayList;
		return studentRepository.findByNameOrderByNameAsc(name);
	}

	@Override
	public Student findByIdOrTech(int id, String tech) {
		return studentRepository.findByIdOrTech(id, tech);
	}

	@Override
	public Student findByIdAndName(int id, String name) {
		return studentRepository.findByIdAndName(id, name);
	}

	@Override
	public Student findByNameLike(String name) {
		return studentRepository.findByNameLike(name);
	}

	@Override
	public long count() {
		return studentRepository.count();
	}
}
