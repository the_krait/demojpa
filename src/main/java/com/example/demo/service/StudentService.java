package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Student;

public interface StudentService {

	public Student save(Student student);
	
	public Student findById(int id);
	
	public Student findByName(String name);
	
	public Student findByTech(String tech);

	public List<Student> findAll();
	
	public List<Student> findByNameOrderByNameAsc(String name);

	public Student findByIdOrTech(int id,String tech);

	public Student findByIdAndName(int id,String name);
	
	public Student findByNameLike(String name);
	
	public long count();
}
