package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="STUDENT")
public class Student {

	@Id
	@Column(name="ID")
	@NotNull()
	private int id;
	
	@Column(name="NAME")
	@NotNull()
	private String name;
	
	@Column(name="ADDR")
	@NotNull()
	private String addr;
	
	@Column(name="TECH")
	@NotNull()
	private String tech;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", addr=" + addr + ", tech=" + tech + "]";
	}
}
