package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.example.demo.model.Student;

@Component
public interface StudentRepository extends JpaRepository<Student, Integer>{

	public Student findByName(String name);
	
	public Student findByTech(String tech);
	
	public List<Student> findByNameOrderByNameAsc(String name);
	
	public Student findByIdOrTech(int id,String tech);
	
	public Student findByIdAndName(int id,String name);
	
	public Student findByNameLike(String name);
}
